const RightArrow = document.getElementById("right_Arrow");
const LeftArrow = document.getElementById("left_Arrow");
const container_HTML_Education = document.getElementById(
  "container_HTML_Education"
);
const card_HTML_Education = document.getElementById("card_HTML_Education");

let position = 0;
function RightArrow_Click() {
  if (position * -1 < container_HTML_Education.offsetWidth-card_HTML_Education.offsetWidth*5)
    position -= card_HTML_Education.offsetWidth;
  container_HTML_Education.style.left = `${position}px`;
  console.log(position);
}
function LeftArrow_Click() {
  if (position < 0) {
    position += card_HTML_Education.offsetWidth;
    container_HTML_Education.style.left = `${position}px`;
    console.log(position);
  }
}
